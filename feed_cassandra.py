import textwrap
import cassandra.cluster

from flight_data import read_flight_data

# csv file containing flight data
file_flights = 'data/2007.csv'

def _insert_query_by_day(flight):
    """
        Parameters
        ----------
        flight : Flight namedtuple
                flight line to insert into the table
        Returns
        -------
        query : string
                cql query to be inserted
    """
    query = textwrap.dedent(
        f"""
        INSERT INTO flight_by_day
        (
            day_of_month,
            day_of_week,
            month,
            flight_num,
            scheduled_dep,
            arr_delay,
            dep_delay
        )
        VALUES
        (
            {flight.day_of_month},
            {flight.day_of_week},
            {flight.month},
            {flight.flight_num},
            {flight.scheduled_dep},
            {flight.arr_delay},
            {flight.dep_delay}
        );
        """
    )
    return query


def insert_datastream(keyspace):
    """
    Parameters
    ----------
    keyspace : string
            keyspace to use to connect
    """
    stream = read_flight_data(file_flights)
    cluster = cassandra.cluster.Cluster()
    session = cluster.connect(keyspace)
    for trip in stream:
        query = _insert_query_by_day(trip)
        session.execute(query)
