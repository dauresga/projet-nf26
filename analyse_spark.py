import textwrap
import numpy as np
import scipy as sp
import scipy.stats
import matplotlib.pyplot as plt

from get_rdd import sc, RDD, dict


def plane_class(tail_num):
    """
    Parameters
    ----------
    tail_num : string
            tail_num of a plane
    Returns
    -------
    int
            class of the plane described by tail_num
    """
    v = dict.get(tail_num)
    if not v:
        return 0
    if 0 < v < 1985:
        return 1
    elif 1985 <= v < 1990:
        return 2
    elif 1990 <= v < 1995:
        return 3
    elif 1995 <= v < 2000:
        return 4
    elif 2000 <= v < 2005:
        return 5
    return 6


def delay(f):
    """
    Parameters
    ----------
    f : namedtuple
            flight object
    Returns
    -------
    float 
            delay of the flight 
    """
    return max(max(f.dep_delay, f.arr_delay), 0)


def _comp_mean_std(v):
    """
    Parameters
    ----------
    v : (int, numpy Array) 
            line given by reduceByKey and identified by a key 
    Returns
    -------
    k : int
            key of the line 
    s1 : int
            sum of flight identified by this key
    mean : float 
            mean of delays
    std : float
            standard deviaton of delays
    """
    k, (s1, sd, sd2) = v
    mean = sd / s1
    std = np.sqrt(sd2 / s1 - mean ** 2)
    return k, (s1, mean, std)


def delay_by_year_analysis(RDD):
    """
    Parameters
    ----------
    RDD : Spark RDD
            RDD to map and reduce
    Returns
    -------
    counts : numpy Array of int
            array of counts
    means : numpy Array of float
            array of delays means
    stds : numpy Array of float
            array of delays standard deviation
    """
    delay_by_year = (
        RDD.map(lambda f: (plane_class(f.tail_num), np.array([1, delay(f), delay(f) ** 2])))
        .reduceByKey(lambda x,y: x + y)
        .map(_comp_mean_std)
        .collect()
    )
    counts = np.zeros(7, dtype=int)
    means = np.zeros(7)
    stds = np.zeros(7)
    for (h, (count, mean, std)) in delay_by_year:
        counts[h] = count
        means[h] = mean
        stds[h] = std
    
    return counts, means, stds


def resume(counts, means, stds):
    """
    Parameters
    ----------
    counts : numpy Array of int
            array of counts
    means : numpy Array of float
            array of delays means
    stds : numpy Array of float
            array of delays standard deviation
    Returns
    -------
    resum : string
            summary of data
    pval : float
            final p-value
    """
    global_mean = (counts * means).sum() / counts.sum()
    SSW = ((counts - 1) * stds ** 2).sum()
    MSW = SSW / (counts.sum() - counts.size)
    SSB = (counts * (means - global_mean) ** 2).sum()
    MSB = SSB / (counts.size - 1)
    fobs = MSB / MSW
    pval = sp.stats.f(counts.size - 1, counts.sum() - counts.size).sf(
        fobs
    )  # sf() = 1-cdf()
    resum = textwrap.dedent(
        f"""\
        Inter-moda\t df={counts.size-1:8d}\t sumsq={SSB:g}\t meansq={MSB:g}
        Intra-moda\t df={counts.sum()-counts.size:8d}\t sumsq={SSW:g}\t meansq={MSW:g}\t f={fobs:g}\t pval={pval:g}
        Total     \t df={counts.sum()-1:8d}\t sumsq={SSW+SSB:g}
        """
    )
    return (resum, pval)


def plot_counts(counts):
    """
    Parameters
    ----------
    counts : numpy Array of int
            array of counts
    """
    plt.bar(range(7), counts)
    plt.xlabel('Year classe')
    plt.ylabel('Number of flights')
    plt.title('Number of flights in each class')
    plt.savefig('counts')
    

def plot_means_stds(counts, means, stds):
    """
    Parameters
    ----------
    counts : numpy Array of int
            array of counts
    means : numpy Array of float
            array of delays means
    stds : numpy Array of float
            array of delays standard deviation
    """
    fig, ax = plt.subplots()
    for c in range(7):
        ax.plot([c, c], means[c] + np.array([-1, 1]) * 1.96 * stds[c], "b-")
    ax.plot(range(7), means, "ro")
    plt.xlabel('Year classe')
    plt.title('Mean and dispersion in each class')
    plt.savefig('mean_std')


if __name__ == '__main__':
    c, m, s = delay_by_year_analysis(RDD)
    (resum, p) = resume(c, m, s)
    # plot_counts(c)
    plot_means_stds(c, m, s)
    print(resum)
    print(p)
