import csv
import collections


# for test purposes
limiteur = lambda generator, limit: (data for _, data in zip(range(limit), generator))


# structure to read and iterate over
Flight = collections.namedtuple(
    "Flight",
    ("day_of_month", "day_of_week", "month", "flight_num", "scheduled_dep", "tail_num", "arr_delay", "dep_delay")
)

def read_csv(filename):
    """
    Parameters
    ----------
    filename : string
            path of csv file containing flight data
    Yields
    -------
    Flight : namedtuple
            line read in csv file encapsulated in a namedtuple
    """
    with open(filename) as f:
        for row in csv.DictReader(f):
            day_of_month = int(row["DayofMonth"])
            day_of_week = int(row["DayOfWeek"])
            month = int(row["Month"])
            flight_num = int(row["FlightNum"])
            scheduled_dep = int(row["CRSDepTime"])
            tail_num = row["TailNum"]
            arr_delay = -1 if row["ArrDelay"] == "NA" else int(row["ArrDelay"])
            dep_delay = -1 if row["DepDelay"] == "NA" else int(row["DepDelay"])
            yield Flight(day_of_month, day_of_week, month, flight_num, scheduled_dep, tail_num, arr_delay, dep_delay)


def read_flight_data(filename, limit=None):
    """
    Parameters
    ----------
    filename : string
            path of csv file containing flight data
    limit : int, optional
            number of lines to read, if None, all file is read
    Returns
    -------
    limiteur or gen : generator
            generator containing read lines
    """
    gen = read_csv(filename)
    if limit is None:
        return gen
    return limiteur(gen, limit)


def read_plane_data(filename):
    """
    Parameters
    ----------
    filename : string
            path of csv file containing plane data
    Returns
    -------
    dict_plane : dictionnary
            dictionnary in format (k,v) = (tailnum, year)
    """
    dict_plane = {}
    with open(filename) as f:
        for row in csv.DictReader(f):
            dict_plane[row["tailnum"]] = -1 if (row["year"] is None or row["year"] == 'None') else int(row["year"])

    return dict_plane
