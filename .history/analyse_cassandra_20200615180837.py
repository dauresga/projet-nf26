import textwrap
import cassandra
import numpy as np
import cassandra.cluster
import matplotlib.pyplot as plt

from flight_data import Flight
from feed_cassandra import insert_datastream

keyspace = 'dauresga_project'


def delay(f):
    """
    Parameters
    ----------
    f : namedtuple
            flight object
    Returns
    -------
    float 
            delay of the flight 
    """
    return max(max(f.dep_delay, f.arr_delay), 0)


class FlightData:
    def __init__(self, keyspace):
        self._cluster = cassandra.cluster.Cluster()
        self._session = self._cluster.connect(keyspace)

    def get_flight_week_month(self, day_week, month):
        """
        Parameters
        ----------
        day_week : int
                day of the week from 1 to 7
        month : int
                month from 1 to 12
        Yields
        -------
        Flight : namedtuple 
        """
        query = textwrap.dedent(
            f"""
            SELECT
            day_of_month, day_of_week, month, flight_num, scheduled_dep, arr_delay, dep_delay
            FROM
            flight_by_day
            WHERE
                    day_of_week={day_week}
                AND
                    month={month}
            ;
            """
        )
        for r in self._session.execute(query):
            yield Flight(
                r.day_of_month,
                r.day_of_week,
                r.month,
                r.flight_num,
                r.scheduled_dep,
                0,
                r.arr_delay,
                r.dep_delay,
            )


    def get_flight_by_month(self, month):
        """
        Parameters
        ----------
        month : int
                month from 1 to 12
        Returns
        -------
        result : dictionnary
                dictionnary with format (k,v) = (day,mean) 
        """
        dict_day = {day:[0,0] for day in range(1,8)}
        for day in range(1,8):
            for x in self.get_flight_week_month(day, month):
                dict_day[day][0] += 1
                dict_day[day][1] += delay(x)

        result = {}
        for k, v in dict_day.items():
            result[k] = v[1]/v[0] if v[0] != 0 else 0

        plt.bar(result.keys(), result.values())
        plt.savefig('by_month')

        return result


    def get_flight_by_week_day(self, day):
        """
        Parameters
        ----------
        day_week : int
                day of the week from 1 to 7
        Returns
        -------
        result : dictionnary
                dictionnary with format (k,v) = (month,mean) 
        """
        dict_month = {month:[0,0] for month in range(1,13)}
        for month in range(1,13):
            for x in self.get_flight_week_month(day, month):
                dict_month[month][0] += 1
                dict_month[month][1] += delay(x)

        result = {}
        for k, v in dict_month.items():
            result[k] = v[1]/v[0] if v[0] != 0 else 0
        
        plt.bar(result.keys(), result.values())
        plt.savefig('by_day')

        return result
    

    def plot_best_week_day(self):
        """
        Returns
        -------
        result : dictionnary
                dictionnary with format (k,v) = (day,mean)  
        """
        dict_day = {day:[0,0] for day in range(1,8)}
        for day in range(1,8):
            for month in range(1,13):
                for x in self.get_flight_week_month(day, month):
                    dict_day[day][0] += 1
                    dict_day[day][1] += delay(x)
        print(dict_day)

        result = {}
        for k, v in dict_day.items():
            result[k] = v[1]/v[0] if v[0] != 0 else 0
        
        plt.bar(result.keys(), result.values())
        plt.title('Average delay per day of week')
        plt.xlabel('Day of week')
        plt.ylabel('Average delay')
        plt.savefig('plot_by_day')

        return result
    
    def plot_best_month(self):
        """
        Returns
        -------
        result : dictionnary
                dictionnary with format (k,v) = (month,mean)  
        """
        dict_month = {month:[0,0] for month in range(1,13)}
        for month in range(1,13):
            for day in range(1,8):
                for x in self.get_flight_week_month(day, month):
                    dict_month[month][0] += 1
                    dict_month[month][1] += delay(x)
        print(dict_month)

        result = {}
        for k, v in dict_month.items():
            result[k] = v[1]/v[0] if v[0] != 0 else 0
        
        plt.bar(result.keys(), result.values())
        plt.title('Average delay per month')
        plt.xlabel('Month')
        plt.ylabel('Average delay')
        plt.savefig('plot_by_month')

        return result


if __name__ == '__main__':
    # insert_datastream(keyspace)
    flight_data = FlightData(keyspace)
    # day = flight_data.plot_best_week_day()
    month = flight_data.plot_best_month()
