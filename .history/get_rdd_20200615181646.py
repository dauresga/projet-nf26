import pyspark

from flight_data import read_plane_data, read_flight_data


# csv files containing flight and plane data
file_flights = 'data/2007.csv'
file_planes = 'data/plane-data.csv'


def get_RDD_from_flights(filename, limit=None, sc=None, numSlices=None):
    """
    Parameters
    ----------
    filename : string
            path of csv file containing flight data
    limit : int, optional
            number of lines to read, if None, all file is read
    sc : SparkContext, optional
            SparkContext to reuse, if None, creates one
    numSlices : int, optional
            number of slices to use when parallelizing, if None, set to 1000
    Returns
    -------
    sc : SparkContext
    D : Spark RDD
    """
    if sc is None:
        sparkconf = pyspark.SparkConf()
        sparkconf.set('spark.port.maxRetries', 128)
        sc = pyspark.SparkContext(conf=sparkconf)
    if numSlices is None:
        numSlices = 1000
    D = sc.parallelize(read_flight_data(filename, limit), numSlices=numSlices)
    return sc, D


sc, RDD = get_RDD_from_flights(file_flights)
dict = read_plane_data(file_planes)
